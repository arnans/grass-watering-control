# Grass watering control

An MQTT-controlled 4xrelay module. Controls a water pump and a 12V electric valve. Uses two relays to form an H-Bridge for the valve. 
  